import React, { Component, PropTypes } from 'react';
import Drawer from 'material-ui/Drawer';
import { List, ListItem, makeSelectable } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import { spacing, typography, zIndex } from 'material-ui/styles';
import { blue600, } from 'material-ui/styles/colors';

import GiftCardIcon from 'material-ui/svg-icons/action/card-giftcard';
import OmniCardIcon from 'material-ui/svg-icons/action/card-membership';
import GoWalletIcon from 'material-ui/svg-icons/action/account-balance-wallet';
import OrderCentralIcon from 'material-ui/svg-icons/action/shopping-cart';
import PayGoIcon from 'material-ui/svg-icons/action/receipt';
import RibsIcon from 'material-ui/svg-icons/action/work';
import KeystoneIcon from 'material-ui/svg-icons/action/account-balance';
import AndroidIcon from 'material-ui/svg-icons/hardware/phone-android';

const SelectableList = makeSelectable(List);

const styles = {
  logo: {
    cursor: 'pointer',
    fontSize: 16,
    color: typography.textFullWhite,
    lineHeight: `${spacing.desktopKeylineIncrement}px`,
    fontWeight: typography.fontWeightLight,
    backgroundColor: blue600,
    paddingLeft: spacing.desktopGutter,
    marginBottom: 8,
  },
  version: {
    paddingLeft: spacing.desktopGutterLess,
    fontSize: 16,
  },
};

class AppNavDrawer extends Component {
  static propTypes = {
    docked: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
    onChangeList: PropTypes.func.isRequired,
    onRequestChangeNavDrawer: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    style: PropTypes.object,
  }; keystone

  static contextTypes = {
    muiTheme: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
  };

  state = {
    muiVersions: [],
  };

  handleRequestChangeLink = (event, value) => {
    window.location = value;
  };

  handleTouchTapHeader = () => {
    this.context.router.push('/');
    this.props.onRequestChangeNavDrawer(false);
  };

  render() {
    const {
      location,
      docked,
      onRequestChangeNavDrawer,
      onChangeList,
      open,
      style,
    } = this.props;

    return (
      <Drawer
        style={style}
        docked={docked}
        open={open}
        onRequestChange={onRequestChangeNavDrawer}
        containerStyle={{ zIndex: zIndex.drawer - 100 }}
        >
        <div style={styles.logo} onTouchTap={this.handleTouchTapHeader}>
          BHN
        </div>

        <SelectableList
          value=""
          onChange={this.handleRequestChangeLink}
          >
          <Subheader>Service Portals</Subheader>
          <ListItem
            primaryText="Telecom"
            primaryTogglesNestedList={true}
            nestedItems={[
              <ListItem primaryText="Blisslite" value="/#/fintel" />,
              <ListItem primaryText="Gowallet" value="/#/fintel" />,
              <ListItem primaryText="RIBS" value="/#/fintel" />,
            ]}
            />
          <ListItem primaryText="Processor"
            primaryTogglesNestedList={true}
            nestedItems={[
              <ListItem primaryText="keystone" value="/#/fintel" />,
              <ListItem primaryText="Paygo" value="/#/fintel" />,
              <ListItem primaryText="FIS" value="/#/fintel" />,
            ]}
            />
          <ListItem primaryText="Aquiring"
            primaryTogglesNestedList={true}
            nestedItems={[
              <ListItem primaryText="RIBS" value="/#/ribs" />
            ]}
            />
          <ListItem primaryText="Fulfillment"
            primaryTogglesNestedList={true}
            nestedItems={[
              <ListItem primaryText="Order Central"
                primaryTogglesNestedList={true}
                nestedItems={[
                  <ListItem primaryText="Fulfillment" value="/#/gc" />,
                  <ListItem primaryText="Customer Serivce" value="/#/omni" />,
                  <ListItem primaryText="Dropship" value="/#/gc" />,
                  <ListItem primaryText="Fulfillment Request" value="/#/gc" />,
                  <ListItem primaryText="Application Manager" value="/#/gc" />
                ]}
                />

            ]}
            />
          <ListItem primaryText="Ecommerce"
            primaryTogglesNestedList={true}
            nestedItems={[
              <ListItem primaryText="GC" value="/#/gc" />,
              <ListItem primaryText="Omni" value="/#/omni" />,
              <ListItem primaryText="Cardlab" value="/#/gc" />,
            ]}
            />
          <ListItem primaryText="Knowledgeable"
            primaryTogglesNestedList={true}
            nestedItems={[
              <ListItem primaryText="PHPKB" value="/#/gc" />,
              <ListItem primaryText="ServiceNow" value="https://bhn.service-now.com/" />
            ]}
            />
        </SelectableList>
      </Drawer>
    );
  }
}

export default AppNavDrawer;
