import React, { Component } from 'react';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import Toggle from 'material-ui/Toggle';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    border: '1px solid #E0E0E0',
    flex: 1
  },
};

class Filter extends Component {
  
  render() {
    return (
      <div style={styles.root}>

        <List>
          <Subheader>Filter</Subheader>
          <Divider />
          <ListItem
            leftCheckbox={<Checkbox />}
            primaryText="Fulfilled"
            />
          <ListItem
            leftCheckbox={<Checkbox />}
            primaryText="Shipped"
            />
          <ListItem
            leftCheckbox={<Checkbox />}
            primaryText="Rejected"
            />
        </List>
      </div>
    );
  }
}

export default Filter;