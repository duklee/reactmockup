import React, { Component } from 'react';
import Title from 'react-title-component';
import MarkdownElement from '../../MarkdownElement';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import FontIcon from 'material-ui/FontIcon';

import aboutText from './Gowallet.md';
import Filter from './Filter';

const iconStyles = {
  marginRight: 24,
};

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  side1: {
    flex: 1,
    order: 2,
    paddingTop: 20
  },
  main: {
    flex: 4,
    order: 3,
    paddingLeft: 40
  }
};

class Gowallet extends Component {

  render() {
    return (
      <div style={styles.root}>
        <Title render={(previousTitle) => `About - ${previousTitle}`} />
        <MarkdownElement text={aboutText} />
        <div style={styles.side1}>
          <Filter />
        </div>
        <div style={styles.main}>
          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn>ID</TableHeaderColumn>
                <TableHeaderColumn>Name</TableHeaderColumn>
                <TableHeaderColumn>Status</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
              <TableRow>
                <TableRowColumn>1</TableRowColumn>
                <TableRowColumn>John Smith</TableRowColumn>
                <TableRowColumn>Employed</TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>2</TableRowColumn>
                <TableRowColumn>Randal White</TableRowColumn>
                <TableRowColumn>Unemployed</TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>3</TableRowColumn>
                <TableRowColumn>Stephanie Sanders</TableRowColumn>
                <TableRowColumn>Employed</TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>4</TableRowColumn>
                <TableRowColumn>Steve Brown</TableRowColumn>
                <TableRowColumn>Employed</TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}

export default Gowallet;


