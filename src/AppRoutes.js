import React from 'react';
import {
  Route,
  Redirect,
  IndexRoute,
} from 'react-router';

// Here we define all our material-ui ReactComponents.
import Master from './components/Master';
import Home from './components/pages/Home';
import Fintel from './components/portals/fintel/Fintel';
import GC from './components/portals/gc/GC';
import Gowallet from './components/portals/gowallet/Gowallet';
import Keystone from './components/portals/keystone/Keystone';
import Omni from './components/portals/omni/Omni';
import Ordercentral from './components/portals/ordercentral/Ordercentral';
import Paygo from './components/portals/paygo/Paygo';
import Ribs from './components/portals/ribs/Ribs';

/**
 * Routes: https://github.com/reactjs/react-router/blob/master/docs/API.md#route
 *
 * Routes are used to declare your view hierarchy.
 *
 * Say you go to http://material-ui.com/#/components/paper
 * The react router will search for a route named 'paper' and will recursively render its
 * handler and its parent handler like so: Paper > Components > Master
 */
const AppRoutes = (
  <Route path="/" component={Master}>
    <IndexRoute component={Home} />
    <Route path="home" component={Home} />
    <Route path="fintel" component={Fintel} />
    <Route path="gc" component={GC} />
    <Route path="gc" component={GC} />
    <Route path="gowallet" component={Gowallet} />
    <Route path="keystone" component={Keystone} />
    <Route path="omni" component={Omni} />
    <Route path="ordercentral" component={Ordercentral} />
    <Route path="paygo" component={Paygo} />
    <Route path="ribs" component={Ribs} />
  </Route>
);

export default AppRoutes;
